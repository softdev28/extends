import 'package:pet/pet.dart' as pet;

abstract class Pet {
  void name();
  void eat();
  void bite();
  void run();
  void fourleg();
  void hunter();
}

abstract class Reptile {
  //สัตว์เลื้อยคลาน
  void name();
  void eat();
  void bite();
  void wiggle(); //เลื้อย
  void notleg();
  void hunter();
}

abstract class Poultry {
  //สัตว์ปีก
  void name();
  void eat();
  void fly();
  void wings();
  void twoleg();
  void hunter();
}

abstract class Aquatic {
  //สัตว์น้ำ
  void name();
  void eat();
  void swim();
  void notleg();
  void hunter();
}

//--------------------------------------------------------------------------------------------------//
class Dog extends Pet {
  //สุนัข
  @override
  void name() {
    print(" dog name Romeo ");
  }

  void bark() {
    print(" Dog bark is Hong Hong ");
  }

  @override
  void bite() {
    print(" Dog can bite ");
  }

  @override
  void eat() {
    print(" Dog eat dog pellet food ");
  }

  @override
  void fourleg() {
    print(" Dog has fourleg ");
  }

  @override
  void run() {
    print(" Dog can run ");
  }

  void swim() {
    print(" Dog can swim ");
  }

  @override
  void hunter() {
    print(" dog can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Cat extends Pet {
  //เเมว
  @override
  void name() {
    print(" cat name Kwao ");
  }

  @override
  void bite() {
    print(" Cat can bite ");
  }

  @override
  void eat() {
    print(" Cat eat cat pellet food ");
  }

  @override
  void fourleg() {
    print(" Cat has fourleg ");
  }

  @override
  void run() {
    print(" Cat can run ");
  }

  @override
  void hunter() {
    print(" cat can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Snake extends Reptile {
  //งู
  @override
  void name() {
    print(" snake name Handsome ");
  }

  @override
  void bite() {
    print(" Snake can bite ");
  }

  @override
  void eat() {
    print(" Snake eat rat ");
  }

  @override
  void notleg() {
    print(" snake without legs ");
  }

  @override
  void wiggle() {
    print(" snake can wiggle ");
  }

  void swim() {
    print(" snake can swim ");
  }

  @override
  void hunter() {
    print(" snake can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Bird extends Poultry {
  //นก
  @override
  void name() {
    print(" bird name Pikga Boo ");
  }

  @override
  void eat() {
    print(" Bird eat worm ");
  }

  @override
  void fly() {
    print(" Bird can fly ");
  }

  @override
  void twoleg() {
    print(" Bird has twoleg ");
  }

  @override
  void wings() {
    print(" Bird has wing ");
  }

  @override
  void hunter() {
    print(" bird can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Parrot extends Poultry {
  //นกเเก้ว

  @override
  void name() {
    print(" parrot name Tim ");
  }

  @override
  void eat() {
    print(" Parrot eat sunflower seeds ");
  }

  @override
  void fly() {
    print(" Parrot can fly ");
  }

  @override
  void twoleg() {
    print(" Parrot has twoleg ");
  }

  @override
  void wings() {
    print(" Parrot has wings ");
  }

  void speak() {
    print(" Sawadee kub");
  }

  @override
  void hunter() {
    print(" parrot can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Duck extends Poultry {
  //เป็ด

  @override
  void name() {
    print(" duck name FIFA ");
  }

  @override
  void eat() {
    print(" Duck eat small aquatic animals "); //สัตว์น้ำขนาดเล็ก
  }

  @override
  void fly() {
    print(" Duck can fly ");
  }

  @override
  void twoleg() {
    print(" Duck has twoleg ");
  }

  @override
  void wings() {
    print(" Duck has wings ");
  }

  void swim() {
    print(" Duck can swim ");
  }

  @override
  void hunter() {
    print(" duck can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Fish extends Aquatic {
  //ปลา
  @override
  void name() {
    print(" fish name Thongdee ");
  }

  @override
  void eat() {
    print(" fish eat warm ");
  }

  @override
  void notleg() {
    print(" fish without legs ");
  }

  @override
  void swim() {
    print(" fish can swim ");
  }

  @override
  void hunter() {
    print(" fish can hunt ");
  }
}

//--------------------------------------------------------------------------------------------------//
class Shark extends Aquatic {
  //ฉลาม

  @override
  void name() {
    print(" shark name Anva ");
  }

  @override
  void eat() {
    print(" shark eat fish ");
  }

  @override
  void notleg() {
    print(" shark without legs ");
  }

  @override
  void swim() {
    print(" shark can swim ");
  }

  @override
  void hunter() {
    print(" shark can hunt ");
  }
}

void main(List<String> arguments) {
  Shark shark = Shark();
  shark.eat();

  Dog dog = Dog();
  dog.name();
  dog.eat();
  dog.bite();
  dog.run();
  dog.fourleg();
  dog.hunter();
  dog.swim();
  dog.bark();

  Cat cat = Cat();
  cat.name();
  cat.eat();
  cat.bite();
  cat.run();
  cat.fourleg();
  cat.hunter();

  Snake snake = Snake();
  snake.name();
  snake.eat();
  snake.bite();
  snake.notleg();
  snake.hunter();
  snake.wiggle();

  Bird bird = Bird();
  bird.name();
  bird.eat();
  bird.fly();

  Duck duck = Duck();
  duck.swim();

  Parrot parrot = Parrot();
  parrot.speak();

  Fish fish = Fish();
  fish.hunter();
}
